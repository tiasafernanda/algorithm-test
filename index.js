// -   `averagePair([-1, 0, 3, 4, 5, 6], 4.1)` => false
// -   `averagePair([1, 2, 3], 2.5)` => true
// -   `averagePair([1, 3, 3, 5, 6, 7, 10, 12, 19], 8)` => true
// -   `averagePair([1, 2, 5], 2)` => false

function averagePair(arr, num) {
  for (let i = 0; i < arr.length; i++) {
    // console.log(arr[i]);
    for (let x = i + 1; x < arr.length; x++) {
      console.log(arr[x]);
      if ((arr[i] + arr[x]) / 2 == num) {
        // console.log(arr[i]);
        // console.log(arr[x]);
        return true;
      }
    }
  }
  return false;
}

console.log(averagePair([-1, 0, 3, 4, 5, 6], 4.1));
console.log(averagePair([1, 2, 3], 2.5));
console.log(averagePair([1, 3, 3, 5, 6, 7, 10, 12, 19], 8));
console.log(averagePair([1, 2, 5], 2));
