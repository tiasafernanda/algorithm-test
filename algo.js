// - `anagram('aaz', 'zza')` => false
// - `anagram('anagram', 'nagaram'))` => true

function anagram(str1, str2) {
  if (str1.length !== str2.length) {
    return false;
  }

  let result = {};

  for (let i = 0; i < str1.length; i++) {
    let letter = str1[i];
    console.log('letter', letter);

    result[letter] = result[letter] ? (result[letter] += 1) : (result[letter] = 1);
    console.log('result', result[letter]);
    console.log('result+1', (result[letter] += 1));
  }

  for (let i = 0; i < str2.length; i++) {
    let letter = str2[i];

    if (!result[letter]) {
      console.log(result);
      return false;
    } else {
      console.log(result);
      result[letter] -= 1;
      console.log('letterMinus', (result[letter] -= 1));
    }
  }
  return true;
}

console.log(anagram('aaz', 'zza'));
console.log(anagram('anagram', 'nagaram'));
